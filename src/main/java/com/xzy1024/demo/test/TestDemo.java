package com.xzy1024.demo.test;

import com.xzy1024.demo.utils.PdfUtil;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("test")
public class TestDemo {


    @GetMapping("imgToPdf")
    public String imgToPdf(String path, HttpServletRequest request){

        return PdfUtil.imgOfPdf(path, request);
    }
    @GetMapping("excToPdf")
    public String excToPdf(String path, HttpServletRequest request){

        return PdfUtil.exceOfPdf(path);
    }
    @GetMapping("docToPdf")
    public String docToPdf(String path, HttpServletRequest request){

        return PdfUtil.docOfPdf(path);
    }

    @GetMapping("pptToPdf")
    public String pptToPdf(String path, HttpServletRequest request) {

        return PdfUtil.pptOfpdf(path, request);
    }
}
